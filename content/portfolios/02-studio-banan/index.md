---
title: Сайт для компании "Студия Банан"
date: 2023-09-06
slug: studio-banan
permalink: https://banan.studio
cover: /images/content/portfolio/studio-banan.jpg
blurhash: LQI#S.~d0c=~E29mAcxB01J8%$eZ
tags:
  - nuxt
  - wordpress
  - scss
---
