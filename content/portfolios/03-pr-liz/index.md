---
title: Сайт лизинговой компании “Простые решения” входящий в ТОП-40 Европы
date: 2023-05-31
slug: pr-liz
permalink: https://pr-liz.ru
cover: /images/content/portfolio/pr-liz.jpg
blurhash: LON^e;t7?bt7~qofD%WBRkozfPWA
tags:
  - nuxt
  - laravel
  - scss
---
